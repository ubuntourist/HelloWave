#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  wave.py
#
#  Generate graphs and audio playback of common wave forms.
#
#  Requires numpy (scipy would be even better) and sounddevice.
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.04.16
#  _______________________________________________________________
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#  _______________________________________________________________
#
#

import os
import sys
from os.path import expanduser      # Cross-platform home directory finder
from math    import sin, cos, degrees, radians, pi
from time    import *
from wave    import *

import numpy       as np
import sounddevice as sd

__appname__    = "Synthesizer"
__module__     = "Wave"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


def makewaves(frequencies, duration=0.2):
    """Create a wave from a sequence of frequency values and a duration"""
    frames     = int(FPS * duration)  # Number of frames
    harmonics = []                    # A wave table of "harmonics"
    for frequency in frequencies:
        harmonic = []                 # A single pure-ish sine wave
        for x in range(frames):
            sample = sin(x * ((frequency * 2 * pi) / FPS))
            harmonic.append(sample)   # Plot the next data point
        harmonics.append(harmonic)    # Append the harmonic to the wave table
    harmonics = np.array(harmonics)   # Convert the wave table to numpy
    wave = np.sum(harmonics, axis=0)  # MAKE WAVE!
    wave /= len(frequencies)          # Scale the wave back to (-1, 1) range
    return wave

# Sample usage:
#   sound = makewaves(test_tone,   0.5)
#   sd.play(sound, blocking=True)


class Wave:
    """A prototype wave with an empty wave table (flatline)"""

    def __init__(self):
        """y = 0"""
        self.wave = [0] * 360

    def play(self,
             fundamental=PITCH,
             duration=SECONDS,
             resolution=SAMPLES,
             depth=DEPTH):
        """Play the wave - TBD"""
        pass

    def plot(self,
             fundamental=PITCH,
             duration=SECONDS,
             resolution=SAMPLES,
             depth=DEPTH):
        """Plot the wave - TBD"""
        pass

    def scale(self,
              fundamental=PITCH,
              duration=SECONDS,
              resolution=SAMPLES,
              depth=DEPTH,
              scaletype=SCALE):
        """Generate a chromatic scale or major scale"""

        if scaletype == "major":
            self.notes = [fundamental] * 8
            for note in range(1, 8):
                self.notes[note] = self.notes[note - 1] * MAJOR[note - 1]

        elif scaletype == "chromatic":
            self.notes = [fundamental] * 12
            for note in range(1, 12):
                self.notes[note] = self.notes[note - 1] * CHROMATIC[note - 1]

        else:
            print("ERROR: Unknown scale type")
