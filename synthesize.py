#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  synthesize.py
#
#  Generate graphs and audio playback of common wave forms.
#
#  Requires numpy (scipy would be even better) and sounddevice.
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.04.16
#  _______________________________________________________________
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#  _______________________________________________________________
#
#

from os.path import expanduser      # Cross-platform home directory finder
from math    import sin, cos, degrees, radians, pi
from time    import *
import os
import sys

import numpy       as np
import sounddevice as sd

from wave.sine     import *
from wave.triangle import *
from wave.sawtooth import *
from wave.square   import *

__appname__    = "Synthesizer"
__module__     = "wave"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


sd.default.samplerate = FPS


def main():
    _ = os.system("clear")
    print("{0} v.{1}\n{2} ({3})\n{4}, {5} <{6}>\n"
          .format(__appname__,
                  __version__,
                  __copyright__,
                  __license__,
                  __author__,
                  __agency__,
                  __email__))

    # Generate single cycle wave tables for the four common wave types
    sine     = Sine()
    sawtooth = Sawtooth()
    square   = Square()
    triangle = Triangle()

    # Plot them -- does nothing yet
    sine.plot()
    sawtooth.plot()
    square.plot()
    triangle.plot()

    # Play them -- does nothing yet
    sine.play()
    sawtooth.play()
    square.play()
    triangle.play()

    # Test with CSV
    csv = open("waves.csv", "w")
    csv.write('"Sine","Sawtooth","Square","Triangle"\n')
    for degree in range(181, 900):
        csv.write("{0},{1},{2},{3}\n".format(sine.wave[degree % 360],
                                             sawtooth.wave[degree % 360],
                                             square.wave[degree % 360],
                                             triangle.wave[degree % 360]))
    csv.close()
    return 0


if __name__ == "__main__":
    main()
else:
    print(__name__)
