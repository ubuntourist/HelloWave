#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  dtmfa.py
#  Copyright 2015 Kevin Cole <kevin.cole@novawebcoop.org> 2016.01.19
#
#  This code will attempt to generate Dual-Tone Multi-Frequency
#  (DTMF) signals, more commonly known as Touch Tone (tm).
#
#  Requires numpy (scipy would be even better) and sounddevice.
#  _______________________________________________________________
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#  _______________________________________________________________
#

from __future__ import print_function
import sys

import numpy       as np
import sounddevice as sd
import math

from PyQt5.QtCore    import *
from PyQt5.QtGui     import *
from PyQt5.QtWidgets import *

import slimline  # Constructed from Qt Designer & pyside-uic

__appname__    = "Dual-Tone Multi-Frequency Algorithm (DTMFA)"
__author__     = "Kevin Cole"
__copyright__  = "Copyright 2015, NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class Slimline(QDialog, slimline.Ui_Slimline):
    def __init__(self, parent=None):
        """Construct a Dialog window and fill with widgets"""
        super(Slimline, self).__init__(parent)  # Old way. Learn new way.
        self.setupUi(self)

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.showFullScreen()

        self.tone_1.pressed.connect(lambda: self.dial("1"))
        self.tone_2.pressed.connect(lambda: self.dial("2"))
        self.tone_3.pressed.connect(lambda: self.dial("3"))
        self.tone_4.pressed.connect(lambda: self.dial("4"))
        self.tone_5.pressed.connect(lambda: self.dial("5"))
        self.tone_6.pressed.connect(lambda: self.dial("6"))
        self.tone_7.pressed.connect(lambda: self.dial("7"))
        self.tone_8.pressed.connect(lambda: self.dial("8"))
        self.tone_9.pressed.connect(lambda: self.dial("9"))
        self.tone_0.pressed.connect(lambda: self.dial("0"))
        self.tone_star.pressed.connect(lambda: self.dial("*"))
        self.tone_hash.pressed.connect(lambda: self.dial("#"))
        self.tone_A.pressed.connect(lambda: self.dial("A"))
        self.tone_B.pressed.connect(lambda: self.dial("B"))
        self.tone_C.pressed.connect(lambda: self.dial("C"))
        self.tone_D.pressed.connect(lambda: self.dial("D"))

    def dial(self, press):
        sound = makewaves(buttons[press][0], 0.2)
        sd.play(sound, blocking=True)

#  Hz | 1209    1336    1477    1633
# ----+-----------------------------
# 697 |   1       2       3       A
# 770 |   4       5       6       B
# 852 |   7       8       9       C
# 941 |   *       0       #       D


FPS        = 44100               # Frames per second/frameset (bit rate).
sd.default.samplerate = FPS

buttons = {"1": [(697.0, 1209.0), ("",  "",  "")],
           "2": [(697.0, 1336.0), ("A", "B", "C")],
           "3": [(697.0, 1477.0), ("D", "E", "F")],
           "A": [(697.0, 1633.0), ("",  "",  "")],
           "4": [(770.0, 1209.0), ("G", "H", "I")],
           "5": [(770.0, 1336.0), ("J", "K", "L")],
           "6": [(770.0, 1477.0), ("M", "N", "O")],
           "B": [(770.0, 1633.0), ("",  "",  "")],
           "7": [(852.0, 1209.0), ("P", "R", "S")],
           "8": [(852.0, 1336.0), ("T", "U", "V")],
           "9": [(852.0, 1477.0), ("W", "Y", "X")],
           "C": [(852.0, 1633.0), ("",  "",  "")],
           "*": [(941.0, 1209.0), ("",  "",  "")],
           "0": [(941.0, 1336.0), ("",  "",  "")],
           "#": [(941.0, 1477.0), ("",  "",  "")],
           "D": [(941.0, 1633.0), ("",  "",  "")]}

# The following tone info was garnered from:
# http://www.tech-faq.com/frequencies-of-the-telephone-tones.html
#
# Note the attenuation / frequency info is NOT included below...yet.
#
low_tone    = (480.0, 620.0)
high_tone   = (480.0,)
dial_tone   = (350.0, 440.0)
ring_tone   = (440.0, 480.0)            # 2 seconds on, 4 seconds off?
busy_tone   = low_tone                  # On and off every 0.5 seconds
recording   = (1400,)                   # On 0.5 seconds, off 15 seconds
off_hook    = (1400, 2060, 2450, 2600)  # On and off every 0.1 seconds
no_such_num = (200, 400)                # See documentation!!!

test_tone = (261.63,)    # Hz, waves per second, 261.63 = C4-note.


def makewaves(frequencies, duration=0.2):
    frames     = int(FPS * duration)  # Number of frames
    harmonics = []                    # A wave table of "harmonics"
    for frequency in frequencies:
        harmonic = []                 # A single pure-ish sine wave
        for x in range(frames):
            sample = math.sin((x * math.pi) / (FPS / (frequency * 2)))
            harmonic.append(sample)   # Plot the next data point
        harmonics.append(harmonic)    # Append the harmonic to the wave table
    harmonics = np.array(harmonics)   # Convert the wave table to numpy
    wave = np.sum(harmonics, axis=0)  # MAKE WAVE!
    wave /= len(frequencies)          # Scale the wave back to (-1, 1) range
    return wave


def test():
    sound = makewaves(test_tone,   0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(low_tone,    0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(high_tone,   0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(dial_tone,   0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(ring_tone,   0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(busy_tone,   0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(recording,   0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(off_hook,    0.5)
    sd.play(sound, blocking=True)
    sound = makewaves(no_such_num, 0.5)
    sd.play(sound, blocking=True)


def main():
    # Instantiate a form, show it and start the app.
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("{company}")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    app = QApplication(sys.argv)
    app.setOverrideCursor(Qt.PointingHandCursor)
    form = Slimline()
    form.show()
    app.exec_()

#   for press in buttons:
#       sound = makewaves(buttons[press][0], 0.2)
#       sd.play(sound, blocking=True)
#   os.system("clear")
#   print("\nEnter phone numbers. Spaces, dashes and periods ignored.")
#   print("Press [ENTER] on a line by itself to exit the loop.")
#   while True:
#       print("""\nEnter a "phone number" (0-9, *, #, A-D):""")
#       presses = input()
#       presses = presses.upper()
#       if not presses:
#           print("Good-bye")
#           sys.exit()
#       for press in presses:
#           if press not in (" ", ".", "-", "(", ")", "+"):
#               sound = makewaves(buttons[press][0], 0.2)
#               sd.play(sound, blocking=True)
#
#   return 0


if __name__ == "__main__":
    main()
